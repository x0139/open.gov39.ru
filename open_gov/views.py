from django.http import HttpResponse
from django.shortcuts import render

from news.models import Post, PostPress


def index(request):
    qs_news = Post.objects.order_by("-timestamp")[:3]
    qs_press = PostPress.objects.order_by("-timestamp")[:3]
    context = {
        "news": qs_news,
        "press": qs_press,
    }
    return render(request, 'index.html', context)


def search(request):
    return render(request, 'search.html')