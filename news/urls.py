from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin

from .views import (
    news_list,
    news_detail,
    # press_list,
    # press_detail,
)

urlpatterns = [
    url(r'^$', news_list, name='news_list'),
    url(r'^(?P<id>\d+)/$', news_detail, name='news_detail'),
    url(r'^press/$', news_list, {'mode': 'press'}, name='press_list'),
    url(r'^press/(?P<id>\d+)/$', news_detail, {'mode': 'press'}, name='press_detail'),
]
