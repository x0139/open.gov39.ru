# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0002_postpress'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='title',
            field=models.CharField(max_length=255),
        ),
        migrations.AlterField(
            model_name='postpress',
            name='smiTitle',
            field=models.CharField(max_length=255),
        ),
        migrations.AlterField(
            model_name='postpress',
            name='title',
            field=models.CharField(max_length=255),
        ),
    ]
