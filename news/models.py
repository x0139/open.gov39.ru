from django.db import models
from tinymce.models import HTMLField
from django.core.urlresolvers import reverse
# from open_gov.settings import TINYMCE_PROFILE
from . import settings


# Create your models here.

class Post(models.Model):
    title = models.CharField(max_length=255)
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    # content = HTMLField()
    image = models.ImageField(null=True, blank=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("news:news_detail", kwargs={"id": self.id})

    class Meta:
        ordering = ["-timestamp", "-updated"]


class PostPress(models.Model):
    title = models.CharField(max_length=255)
    smiTitle = models.CharField(max_length=255)
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)

    # content = HTMLField()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("news:press_detail", kwargs={"id": self.id})
