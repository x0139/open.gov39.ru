from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from .models import Post, PostPress

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render


# Create your views here.


def news_list(request, mode=None):
    if not mode:
        queryset_list = Post.objects.all()  # .order_by("-timestamp")
    else:
        queryset_list = PostPress.objects.all()  # .order_by("-timestamp")

    paginator = Paginator(queryset_list, 2)  # Show 25 contacts per page
    page_request_var = 'page'
    page = request.GET.get(page_request_var)
    try:
        queryset = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        queryset = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        queryset = paginator.page(paginator.num_pages)

    context = {
        "queryset_list": queryset,
        "page_request_var": page_request_var,
        'range_pagination': range(1, queryset.paginator.num_pages + 1)
    }
    return render(request, 'news_list.html', context)


def news_detail(request, id=None, mode=None):
    if not mode:
        instance = get_object_or_404(Post, id=id)
    else:
        instance = get_object_or_404(PostPress, id=id)
    context = {
        "title": instance.title,
        "instance": instance,
    }
    return render(request, 'news_detail.html', context)
