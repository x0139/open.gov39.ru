#!/usr/bin/env python
# -*- coding: utf-8 -*-
#import telepot
import sys
import httplib
import urllib
import json

def get_token():
   chat_id = None
   try:
      chat_id = sys.argv[1]
   except IndexError:
      pass
   return chat_id

def get_message():
   chat_id = None
   try:
      message = sys.argv[2]
   except IndexError:
      pass
   return message 

def get_project_name():
   p_name = None
   try:
      p_name = sys.argv[3]
   except IndexError:
      pass
   return p_name 

print('Your token: ' + str(get_token()))
conn = httplib.HTTPSConnection('textadept.ru')
headers = {'Content-type': 'application/json'}
params = {"token": get_token(), "message": get_message(), "pname": get_project_name()}
params_json = json.dumps(params)
conn.request("POST", '/commit/project/', params_json, headers)
response = conn.getresponse()
data = response.read()
print(data)



